'use strict';

userApp.component('userDetail', {

    controller: function UserDetailCtrl($routeParams, UsersService) {
        var self = this;
        self.userLoaded = false;
        self.user = UsersService.get({
            userId: $routeParams['userId']
        }, function(successResult) {
            // Окей!
            self.notfoundError = false;
            self.userLoaded = true;

            self.activeTab = 1;
            self.disableControlTab = true;
        }, function(errorResult) {
            // Не окей..
            self.notfoundError = true;
            self.userLoaded = true;


        });

        self.user.$promise.then(function(result) {
            //$scope.userLoaded = true;
        });

        self.deleteUser = function(userId) {

            self.user.$delete({
                userId: userId
            }, function(successResult) {
                // Окей!
                self.deletionSuccess = true;
            }, function(errorResult) {
                // Не окей..
                self.deletionError = true;
            });

        }


    },

    templateUrl: './src/UserDetail/UserDetail.html'

});
