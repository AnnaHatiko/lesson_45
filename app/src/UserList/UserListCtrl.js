'use strict';

userApp.controller('UserListCtrl', function($scope, UsersService) {

    $scope.users = UsersService.query();

});